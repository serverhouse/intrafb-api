# Utilisation de l’API IntraFB e-convoyage


**Chacune des requêtes décrites dans cette documentation devront être préfixées de l’URL https://intrafb.com/api/v1 et devront contenir un paramètre `api_token` ou un header `Api-Token` correspondant à votre clé de connexion que vous pourrez trouver dans la partie « Récupération de la clé d’API ».**

------

## Récupération de la clé d’API

Connectez-vous sur votre espace personnel https://intrafb.com et rendez-vous à l’adresse https://intrafb.com/compte/cle-api pour récupérer ou créer une clé d’API.

Cette clé d’API vous sera utile pour chacune des requêtes qui seront effectuées par la suite : ajoutez simplement un paramètre `api_token` ou un header `Api-Token` dans vos requêtes pour être authentifié.

-----

## Recherche de convoyage

### Récupération d’une liste de convoyages  (GET /convoyage)

Vous avez la possibilité de récupérer les informations partielles d’une liste de convoyage.
Pour ce faire exécutez une requête GET avec si besoin des paramètres de recherche sur les champs :

* `reference` (string) : La référence du convoyage
* `reference_externe` (string) : Votre propre référence
* `id_societe` (int) : L'id de la société liée au convoyage
* `societe` (string) : Le nom de la société liée au convoyage
* `marque` (string) : La marque de la voiture
* `modele` (string) : Le modèle de la voiture
* `immatriculation` (string) : L'immatriculation de la voiture
* `date_apres` (date) : Les convoyages avec une date de prestation supérieure à cette date
* `date_avant` (date): Les convoyages avec une date de prestation inférieure à cette date
* `statut` (string | tableau de string) : Un regroupement de statut du convoyage
    * `demande` : Les demandes effectuées pas encore confirmées, ce groupe contiendra les convoyages avec le statut :
        * `Commande envoyée` : La commande a été envoyée à Fullcar Services en est en attente de validation
    * `en_cours` : Les convoyages en cours, ce groupe contiendra les convoyages avec le statut :
        * `Confirmé` : La commande a été validée par Fullcar Services
        * `En cours` : Le convoyage va bien démarré, le convoyeur va bientôt enlever le véhicule
        * `Véhicule enlevé` : Le véhicule a été enlevé par le convoyeur
        * `Véhicule sur parc` : Le véhicule a été déposé sur un parc de stockage par le convoyeur (Seulement pour un convoyage mixte * Route - Camion)
        * `Véhicule sorti parc` : Le véhicule a été retiré sur un parc de stockage par le convoyeur (Seulement pour un convoyage mixte Route - Camion)
        * `Véhicule livré` : Le véhicule a été livré par le convoyeur
        * `Véhicule restitué` : Le véhicule a été restitué par le convoyeur (Similaire à "Véhicule livré" mais dans le cas d'une restitution)
        * `Incident` : Un incident empêchant la complétion du convoyage s'est produit
    * `fini` : Les convoyages terminés, ce groupe contiendra les convoyages avec le statut :
        * `Terminé` : Le convoyage est terminé
        * `En attente du loueur` : Le convoyage est terminé mais le véhicule n'a pas encore été récupéré par le loueur (Seulement pour un convoyage de type e-restitution)
        * `Restitué au loueur` : Le convoyage est terminé est véhicule a été récupéré par le loueur (Seulement pour un convoyage de type e-restitution)
    * `annule` : Les convoyages annulés ou refusés, ce groupe contiendra les convoyages avec le statut :
        * `Commande refusée` : La commande ne peut pas être traitée par Fullcar Services
        * `Annulé` : Le convoyage a été annulé par Fullcar Services
* `commentaire`: Le commentaire transmis à Fullcar Services


Les références, la marque, le modèle seront toujours des recherches "partielles" : si vous recherchez la référence 123, l'API pourrait par exemple vous retourner les convoyages "L-123", "L-1234", "R-21234", etc...

Deux autres paramètres peuvent être ajoutés

* `page` (int) : Le numéro de la page a afficher (par défaut 1)
* `per_page` (int) : Le nombre de résultat à retourner (par défaut 20)

Ainsi, une requête GET de type `/?marque=Renault&modele=Twi&debut_date__gte=2017-01-01` vous retournera l'ensemble des convoyages des voitures Renault avec un modèle contenant la chaine de caractère "Twi" et une date de départ supérieure ou égale au 1er janvier 2017.

L’objet JSON retourné est composé de deux sections distinctes :

* `data` : Un tableau d’objet contenant les informations basique des convoyages correspondant à la recherche
* `pagination` : les données de pagination avec le numéro de la page actuelle, le nombre total de résultat, le numéro du premier résultat retourné, le numéro du dernier résultat retourné


### Récupération d’un convoyage en particulier (GET /convoyage/[REFERENCE])

Chaque convoyage est identifié par une référence. Une simple requête GET de type `/L-XXXX` vous permettra de récupérer un objet JSON contenant les informations complètes du convoyage.

L’objet récupéré est composé de l’ensemble des informations du convoyage.
